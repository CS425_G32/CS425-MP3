package FilleSystem;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

public class FileList implements Serializable{
	private String _filename;
	private HashSet<String> storeAddress; // = new ArrayList<String>();
	
	public FileList(String fileName, HashSet<String> storeAddress)
	{
		this._filename = fileName;
		this.storeAddress = storeAddress;
	}


	public String get_filename() {
		return _filename;
	}
	public void set_filename(String _filename) {
		this._filename = _filename;
	}
	public HashSet<String> getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(HashSet<String> storeAddress) {
		this.storeAddress = storeAddress;
	}
	
	
}
