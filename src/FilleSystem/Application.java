package FilleSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
	
	private static String FileName;
	private static String Message;
	
	
//	private AtomicBoolean acceptMore= new AtomicBoolean(true);
//	Application()
//	{
//		this.acceptMore.set(true);
//	}
//	public  void stop()
//	{
//		this.acceptMore.set(false); 
//	}
	
//	 private static BufferedReader stdin;
//	  private static PrintStream os;
//	
	public static void startup(){
		
//		AtomicBoolean acceptMore= new AtomicBoolean(true);
		FileTCPServer fts = new FileTCPServer();
		
		Thread ftsServer = new Thread(fts);
		
		ExecutorService executorService = Executors.newFixedThreadPool(4);
		executorService.submit(ftsServer);
		
		
		
		while(true){
			
			System.out.println("Enter 1 to put the file into SDFS Folder");
			System.out.println("Enter 2 to get the file from SDFS Folder");
			System.out.println("Enter 3 to delete the file from SDFS Folder");
			int userInput;
	    	Scanner a = new Scanner(System.in);
	    	userInput=a.nextInt();
	    	try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch(userInput)
        	{
        	case 1 :				System.out.println("Please Enter the file name which you want to Insert");
        							BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        							try {
									FileName = stdin.readLine();
        							} catch (IOException e) {
        								// TODO Auto-generated catch block
        								e.printStackTrace();
        							}
        							FileTCPClient put = new FileTCPClient();
        							put.putfile(FileName,Message);
        							continue;
        							
        	case 2 :				
        							System.out.println("Please Enter the file name which you want to retrieve");
        							BufferedReader stdinget = new BufferedReader(new InputStreamReader(System.in));
        							try {
        								FileName = stdinget.readLine();
        							} catch (IOException e) {
					// TODO Auto-generated catch block
        								e.printStackTrace();
        							}
        							FileTCPClient get = new FileTCPClient();
        							get.getfile(FileName, Message);
        							continue;
        							
        	case 3:						
        								System.out.println("Please Enter the file name you want to remove");
										BufferedReader stdindel = new BufferedReader(new InputStreamReader(System.in));
										try {
												FileName = stdindel.readLine();
												} catch (IOException e) {
// TODO Auto-generated catch block
													e.printStackTrace();
													}
											FileTCPClient del = new FileTCPClient();
											del.deletefile(FileName, Message);
											continue;
        		
        		
        	case 4:						
        								System.out.println("Did not stop");
        								fts.stop();						
        								executorService.shutdown();
        								break;

        	}
			
			
		}
		
	}
	
	 public static void main(String[] args){
		 
		 
		 
		startup();
		
		
	}
	
	
}